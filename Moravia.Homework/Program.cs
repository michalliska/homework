﻿using System.IO;
using Moravia.Homework.Factories;
using Moravia.Homework.Services;

namespace Moravia.Homework
{
	class Program
	{
		static void Main(string[] args)
		{
			var sourceFileName = @"C:\tmp\Document3.xml";
			var targetFileName = @"C:\tmp\Document3.json";

			var sourceDocumentService = DocumentServiceFactory.Create(Path.GetExtension(sourceFileName));
			var targetDocumentService = DocumentServiceFactory.Create(Path.GetExtension(targetFileName));

			var storageService = new FileSystemStorageService();

			var convertService = new ConvertTextFileService(storageService, storageService, sourceDocumentService,
				targetDocumentService);

			convertService.Convert(sourceFileName, targetFileName);
		}
		
	}

	//class Program
	//{
	//	static void Main(string[] args)
	//	{
	//		// not a safe path
	//		var sourceFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\SourceFiles\\Document1.xml");
	//		var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\TargetFiles\\Document1.json");
	//		try
	//		{
	//			// not closing stream, use using
	//			FileStream sourceStream = File.Open(sourceFileName, FileMode.Open);
	//			var reader = new StreamReader(sourceStream);
	//			string input = reader.ReadToEnd();
	//		}
	//		catch (Exception ex)
	//		{
	//			// just rethrowing an exception, losing info, logging
	//			throw new Exception(ex.Message);
	//		}

	//		// undefined input, is it actually xml?, potential exceptions
	//		var xdoc = XDocument.Parse(input);
	//		var doc = new Document
	//		{
	//			// potential null ref exception, case sensitivity
	//			Title = xdoc.Root.Element("title").Value,
	//			Text = xdoc.Root.Element("text").Value
	//		};
	//		var serializedDoc = JsonConvert.SerializeObject(doc);
	//		// potential exceptions, again not closing stream
	//		var targetStream = File.Open(targetFileName, FileMode.Create, FileAccess.Write);
	//		var sw = new StreamWriter(targetStream);
	//		sw.Write(serializedDoc);
	//	}
	//}
}
