﻿using System;
using Moravia.Homework.Services;

namespace Moravia.Homework.Factories
{
	public static class DocumentServiceFactory
	{
		public static IDocumentService Create(string fileExtension)
		{
			switch (fileExtension)
			{
				case ".json":
					return new JsonDocumentService();
				case ".xml":
					return new XmlDocumentService();
				default:
					throw new Exception($"Unsupported file type {fileExtension}.");
			}
		}
	}
}
