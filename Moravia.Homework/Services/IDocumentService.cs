﻿using Moravia.Homework.Model;

namespace Moravia.Homework.Services
{
	public interface IDocumentService
	{
		Document Read(string fileContent);
		string GetString(Document document);

	}
}
