﻿using System.IO;

namespace Moravia.Homework.Services
{
	public class FileSystemStorageService : IStorageService
	{
		public string Read(string fileName)
		{
			return File.ReadAllText(fileName);
		}

		public void Write(string fileName, string fileContent)
		{
			// danger of overriding existing file
			File.WriteAllText(fileName, fileContent);
		}
	}
}
