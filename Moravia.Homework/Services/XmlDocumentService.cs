﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using Moravia.Homework.Model;

namespace Moravia.Homework.Services
{
	public class XmlDocumentService : IDocumentService
	{
		public static string SupportedFileType = ".xml";
		public Document Read(string fileContent)
		{
			try
			{
				var xDoc = XDocument.Parse(fileContent);

				// simple doc format check, depends on specs
				var titleElement = xDoc.Root.Elements().FirstOrDefault(el => el.Name.LocalName.ToLower() == "title");
				var textElement = xDoc.Root.Elements().FirstOrDefault(el => el.Name.LocalName.ToLower() == "text");

				if (titleElement == null || textElement == null)
					throw new Exception("XML missing key element title or text.");

				var doc = new Document
				{
					Title = titleElement.Value,
					Text = textElement.Value
				};

				return doc;
			}
			catch
			{
				// log exception, convey message to user
			}

			return null;
		}

		public string GetString(Document document)
		{
			var xDoc = new XDocument();

			using (var writer = xDoc.CreateWriter())
			{
				var serializer = new DataContractSerializer(document.GetType());
				serializer.WriteObject(writer, document);
			}

			return xDoc.ToString();
		}
	}
}
