﻿namespace Moravia.Homework.Services
{
	public interface IStorageService
	{
		string Read(string fileName);
		void Write(string fileName, string fileContent);
	}
}
