﻿using Moravia.Homework.Model;
using Newtonsoft.Json;

namespace Moravia.Homework.Services
{
	public class JsonDocumentService : IDocumentService
	{
		public Document Read(string fileContent)
		{
			return (Document) JsonConvert.DeserializeObject(fileContent, typeof(Document));
		}

		public string GetString(Document document)
		{
			return JsonConvert.SerializeObject(document);
		}
	}
}
