﻿namespace Moravia.Homework.Services
{
	public class ConvertTextFileService
	{
		private IStorageService SourceStorageService { get; }
		private IStorageService TargetStorageService { get; }
		private IDocumentService SourceDocumentService { get; }
		private IDocumentService TargetDocumentService { get; }
		
		public ConvertTextFileService(IStorageService sourceStorageService, IStorageService targetStorageService, IDocumentService sourceDocumentService,
			IDocumentService targetDocumentService)
		{
			SourceStorageService = sourceStorageService;
			TargetStorageService = targetStorageService;
			SourceDocumentService = sourceDocumentService;
			TargetDocumentService = targetDocumentService;
		}

		public void Convert(string sourceFileName, string targetFileName)
		{
			var fileContent = SourceStorageService.Read(sourceFileName);
			var doc = SourceDocumentService.Read(fileContent);
			var convertedContent = TargetDocumentService.GetString(doc);
			TargetStorageService.Write(targetFileName, convertedContent);
		}
	}
}
