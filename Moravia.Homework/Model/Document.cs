﻿namespace Moravia.Homework.Model
{
	public class Document
	{
		public string Title { get; set; }
		public string Text { get; set; }
	}
}
