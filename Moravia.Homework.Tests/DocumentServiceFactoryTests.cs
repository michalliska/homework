﻿using Moravia.Homework.Factories;
using Moravia.Homework.Services;
using NUnit.Framework;

namespace Moravia.Homework.Tests
{
	public class DocumentServiceFactoryTests
	{
		[SetUp]
		public void Setup()
		{

		}

		[Test]
		public void CanCreateJsonService()
		{
			var service = DocumentServiceFactory.Create(".json");
			Assert.AreEqual(service.GetType(), typeof(JsonDocumentService));
		}

		[Test]
		public void ThrowsExceptionForUnknownExtension()
		{
			var unknownExtension = ".akjdhkad";
			Assert.Catch(() => DocumentServiceFactory.Create(unknownExtension));
		}
	}
}
