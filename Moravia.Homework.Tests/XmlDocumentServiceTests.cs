﻿using System.Linq;
using System.Xml.Linq;
using Moravia.Homework.Model;
using Moravia.Homework.Services;
using NUnit.Framework;

namespace Moravia.Homework.Tests
{
	public class XmlDocumentServiceTests
	{
		private XmlDocumentService xmlDocumentService;

		[SetUp]
		public void Setup()
		{
			xmlDocumentService = new XmlDocumentService();
		}

		[Test]
		public void TestRead()
		{
			var title = "bla";
			var text = "bla bla bla";
			var fileContent = $"<Document xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.datacontract.org/2004/07/Moravia.Homework.Model\">\r\n  <Text>{text}</Text>\r\n  <Title>{title}</Title>\r\n</Document>";
			var readDoc = xmlDocumentService.Read(fileContent);
			Assert.AreEqual(title, readDoc.Title);
			Assert.AreEqual(text, readDoc.Text);
		}

		[Test]
		public void TestWrite()
		{
			var title = "bla";
			var text = "bla bla bla";
			var doc = new Document { Text = text, Title = title };
			var read = xmlDocumentService.GetString(doc);

			var xDoc = XDocument.Parse(read);
			var titleElement = xDoc.Root?.Elements().FirstOrDefault(e => e.Name.LocalName == "Title");
			var textElement = xDoc.Root?.Elements().FirstOrDefault(e => e.Name.LocalName == "Text");
			Assert.IsNotNull(titleElement);
			Assert.IsNotNull(textElement);
			Assert.AreEqual(title, titleElement.Value);
			Assert.AreEqual(text, textElement.Value);

			//var expected = $"<Document xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.datacontract.org/2004/07/Moravia.Homework.Model\">\r\n  <Text>{text}</Text>\r\n  <Title>{title}</Title>\r\n</Document>"; 
			//Assert.AreEqual(expected, read);
		}
	}
}
