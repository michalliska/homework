using Moravia.Homework.Model;
using Moravia.Homework.Services;
using NUnit.Framework;

namespace Moravia.Homework.Tests
{
	public class JsonDocumentServiceTests
	{
		private JsonDocumentService jsonDocumentService;

		[SetUp]
		public void Setup()
		{
			jsonDocumentService = new JsonDocumentService();
		}

		[Test]
		public void TestRead()
		{
			var title = "bla";
			var text = "bla bla bla";
			var fileContent = $"{{\"Title\":\"{title}\",\"Text\": \"{text}\"}}";
			var readDoc = jsonDocumentService.Read(fileContent);
			Assert.AreEqual(title, readDoc.Title);
			Assert.AreEqual(text, readDoc.Text);
		}

		[Test]
		public void TestWrite()
		{
			var title = "bla";
			var text = "bla bla bla";
			var doc = new Document { Text = text, Title = title };
			var read = jsonDocumentService.GetString(doc);
			var expected = $"{{\"Title\":\"{title}\",\"Text\":\"{text}\"}}";
			Assert.AreEqual(expected, read);
		}
	}
}